var React = require('react');
var ReactDome = require('react-dom');
var List = require('./components/List.jsx');

ReactDome.render(
	<List title="Ingredients"/>,
	document.getElementById('ingredients')
);